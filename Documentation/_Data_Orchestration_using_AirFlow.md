# Key Observation - Understand what Airflow is and how it creates Data Pipelines

## What is Airflow?

>**Airflow is a platform created by the community to programmatically author, schedule, and monitor workflows.**

## What problems does Airflow solve?

Crons are an age-old way of scheduling tasks.

1. With cron creating and maintaining a relationship between tasks is a nightmare, whereas, in Airflow, it is as simple as writing Python code.

2. Cron needs external support to log, track, and manage tasks. Airflow UI to track and monitor the workflow execution

3. Cron jobs are not reproducible unless externally configured. The Airflow keeps an audit trail of all tasks executed.

4. Scalable

## Airflow Architecture

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/Apache_Airflow_.jpg?inline=false)

## How to define a workflow in Airflow?

**Part I: How to create a DAG and the operators to perform tasks?**

* When we want to create a DAG, we have to give the name, the description, the start date and the interval as you can see the below.

```py
from airflow import DAG

first_dag = DAG( ‘first’, 
description = ‘text’, 
start_date = datetime(2020, 7, 28),
schedule_interval = ‘@daily’)
```

* Operators are the building blocks of DAG. They define the actual work that a DAG will perform. We need to parametrise the operators by setting the task_id, the python_callable and the dag.

```py
from airflow import DAG 

from airflow.operators.python_operator import PythonOperator

def my_func():
   print('done')
                  
first_dag = DAG(...)

task = PythonOperator(
         task_id = 'first_task',
         python_callable=my_func ,
         dag=first_dag)
```

* Other common operators are:
               
                - PostgresOperator
                - RedshiftToS3Operator
                - S3ToRedshiftOperator
                - BashOperator
                - SimpleHttpOperator
                - Sensor

* Schedules are optional and by default, if we omit the schedule interval, our DAG will run once a day from the start day. Anyway, below you can find all the alternatives for setting up your schedule interval.

                @once →run a DAG only once time
                @hourly → run the DAG every hour
                @daily → run the DAG every day
                @weekly → run the DAG every week
                @monthly → run the DAG every month
                @yearly → run the DAG every 
                
**Part II: Task Dependencies and Airflow Hooks**

* In Airflow every Directed Acyclic Graphs is characterized by nodes(i.e tasks) and edges that underline the ordering and the dependencies between tasks.

* We can describe the dependencies by using the double arrow operator ‘>>’. So:

1. a>>b means a comes before b
2. a<<b means b come before a

* An alternative is to use set_downstream and set_upstream:

1. a.set_downstream(b) means a comes before b
2. a.set_upstream(b) means a comes after b

* To interact with our data stores and other external system like Redshift, Postgres or MySQL we can configure Airflow Hooks. Below an example on how it looks like.

```py
from airflow import DAG

from airflow.hooks.postgres_hook import PostgresHook

form airflow.operators.python_operator import PythonOperator

def load():
      #create  a PostgresHook option using the 'example' connection
      db_hook = PostgresHook('example')
      df = db_hook.get_pandas_df('SELECT * FROM my_table')

      load_task = PyhtonOperator(task_id='load',                                  
      python_callable=my_func_name, ...)
```      

Above you can observe that I imported a PostgresHook, and I initiated it by giving the name ‘example’.

* Then with the function db_hook.get_pandas_df(‘SELECT * FROM my_table’) I ask to give me a pandas data frame. So Airflow go off and run the SQL statement and finally the result is loaded into a pandas data frame and returned to Airflow.

However, Airflow have other hooks like:

                 - HttpHook
                 - MySqlHook
                 - SlackHook

**Part III: Context and Templating**

* Airflow provide several context variables specific to the execution of a given DAG and a task at runtime. Context variables are useful in the process of accessing or segmenting data before processing. You can find here a list of variables that can be included as kwargs.

* Below, we have a function called my_func. Into the function we have *args and **kwargs(i.e. the list of keyword arguments). In addition, you can notice that in the task the provide_context is set as True.

```py
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

def my_func(*args, **kwargs): 
         logging_info(f"start{kwargs['ds']}")

first_dag = DAG(...)

task = PythonOperator(
      task_id = 'date',
      python_callable = my_func,
      provide_context = True,
      dag=first_dag)
```      

# Run Airflow on Ubuntu 16.04 using docker

## Prereqisite

* Docker must be [install](https://docs.docker.com/engine/install/ubuntu/) on Ubuntu 16.04
* Docker-compose must be [install](https://docs.docker.com/compose/install/) on ubuntu 16.04
* Docker and docker-compose versions must be [compatible](https://docs.docker.com/compose/compose-file/compose-versioning/) with each other
* I followed [Pluralsight](https://www.pluralsight.com/courses/productionalizing-data-pipelines-apache-airflow) author 'axel sirota' for learning Airflow.

## Run airflow

1. Download github repository form [here](https://github.com/axel-sirota/productionalizing-data-pipelines-airflow) and save it.

2. open terminal in ubuntu and direct your directory to docker-compose.yml file. in this repository this file located  in 'module3-demo1' 

3. Run following command to execute compose file
```sh                  
sudo docker-compose up --build
```

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/start.png?inline=false)

4. Wait for airflow symbol.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/wait.png?inline=false)

5. Checkout [localhost](http://localhost:8080/) port.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/localhost.png?inline=false)                      

# Key Observation - Make pipelines more resilient and predictable

> **Resiliency means having an ability to adapt. Resilient data pipelines adapt in the event of failure. Data pipelines are meant to transport and transform data from one point to another.**

 So, a resilient data pipeline needs to:

1. Detect failures
2. Recover from failures
3. Return accurate data to the consumer

## How to use branches, templates and macros in Apache Airflow?
 
* Templates and Macros in Apache Airflow are the way to pass dynamic data to your DAGs at runtime.

* Let’s imagine that you would like to execute a SQL request using the execution date of your DAG? How can you do that? How could you use the DAG id of your DAG in your script to generate data? Maybe you need to know when your next DagRun will be? How could you get this value in your tasks? Well, all of these questions can be answered using macros and templates.

## Simple example 

1. In the [axel sirota's](https://github.com/axel-sirota/productionalizing-data-pipelines-airflow/tree/main/module5-demo2/docker/airflow/sql) demo file for performing templating there is file called 'exec_report.sql'. in that file you can edit date or you can import date from dag file.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/Template.png?inline=false)

2. For performing macros you can edit the macros in main dag file.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/macros.png?inline=false)

3. finally you can run and observe the result.
   
   * Start dag

   ![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/start_dag.png?inline=false)

   * Observe rendered messaege in data science task.

   ![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/rendered_message.png?inline=false)

   * Observe rendered sql in create report task.

   ![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/rendered_sql.png?inline=false) 

4. Create branch using python branch operator with dummy operator in dag file.

   ![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/python_branch_operator_.png?inline=false)

5. Write branch definition. 

   ![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/branch_definition.png?inline=false)  

6. Define final flow in dag file.    
 
   ![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/define_flow.png?inline=false)

   * Start dag

   * Open graph view of dag and observe the skiped tasked because of branch operator.

   ![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/skiped_task.png?inline=false)

# Key Observation - Distribute tasks with Celery and Kubernetes Executors

## What is an Executor?

Once a DAG is defined (perhaps with the help of an Operator), the following needs to happen in order for a single or set of "tasks" within that DAG to execute and be completed from start to finish:

1. The Metadata Database (in Astronomer, that's PostgreSQL) keeps a record of all tasks within a DAG and their corresponding status (queued, scheduled, running, success, failed, etc) behind the scenes.

2. The Scheduler reads from the Metadatabase to check on the status of each task and decide what needs to get done (and in what order).

3. The Executor works closely with the Scheduler to figure out what resources will actually complete those tasks (via a worker process or otherwise) as they're queued.

The difference between executors comes down to the resources they have at hand and how they choose to utilize those resources to distribute work (or not distribute it at all).

### Local Executor

>**Running Apache Airflow on a LocalExecutor exemplifies single-node architecture.**

**Pros**

* It's straightforward and easy to set up
* It's cheap and resource light
* It still offers parallelism

**Cons**

* It's not (as) scalable
* It's dependent on a single point of failure

**When you should use it**

* The LocalExecutor is ideal for testing.

* The obvious risk is that if something happens to your machine, your tasks will see a standstill until that machine is back up.

* Heavy Airflow users whose DAGs run in production will find themselves migrating from LocalExecutor to CeleryExecutor after some time, but you'll find plenty of use cases out there written by folks that run quite a bit on a LocalExecutor before making the switch. How much that executor can handle fully depends on your machine's resources and configuration. Until all resources on the server are used, the LocalExecutor actually scales up quite well.

### Celery Executor

>**At its core, Airflow's CeleryExecutor is built for horizontal scaling.**

**Pros**

* High availability
* Built for horizontal scaling
* Worker Termination Grace Period (on Astronomer)

**Cons**

* It's pricier
* It takes some work to set up
* Worker maintenance

**When you should use it**

* While the LocalExecutor is a great way to save on engineering resources for testing even with a heavy workload, we generally recommend going with Celery for running DAGs in production, especially if you're running anything that's time sensitive.

### KubernetesExecutor

>**Kubernetes Executor leverages the power of Kubernetes for resource optimization.**

**Pros**

* Cost and resource efficient
* Fault tolerant
* Task-level configurations
* No interruption to running tasks if a deploy is pushed

**Cons**

* Kubernetes familiarity as a potential barrier to entry
* An overhead of a few extra seconds per task for a pod to spin up

**When you should use it**

* The Kubernetes Executor offers extraordinary capabilities. If you're familiar with Kubernetes and want to give it a shot, we'd highly recommend doing so to be at the forefront of the modern Apache Airflow configuration.

## Simple example of local executers

1. According to Axel sirota's sample demo We need to change environment for executer.

2. We need to set environmet for local executers. For that we need to edit docker-compse.yml file.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/Environment_setup_.png?inline=false)

3.Start parallel dag file and monitor graph view.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/Parallel.png?inline=false)

4. Explore Gantt view of parallel dag file. you will see the less than 5 task executed in parallel way.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/local_parallel.png?inline=false)

5. Why less than 5 task because we set concurrency to 5. We can change it also.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/concurrency_rate.png?inline=false)

## Simple example of Celery Executers

1. First we need to add message broker for worker management. we need to change in dockrer-compose.yml file.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/redis_message_broker.png?inline=false)

2. Set flower environment for we need to change docker-compose.yml file.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/flower_for_celery_executor.png?inline=false)

3. Run the docker compose file and monitor task as well as you will see flower dashboard.

![](https://gitlab.com/Gyogeshwar/airflow-summary/-/raw/master/Source/flower.png?inline=false)



## References

[Content Reference](https://towardsdatascience.com/how-to-build-a-data-pipeline-with-airflow-f31473fa42cb)

[Pluralsight Reference](https://www.pluralsight.com/courses/productionalizing-data-pipelines-apache-airflow)



